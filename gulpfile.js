const {src, dest, watch, series, parallel} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const browserSync = require("browser-sync");
const uglify = require("gulp-uglify-es").default;
const imagemin = require("gulp-imagemin");
const cleanCSS = require('gulp-clean-css');
const del = require("del");

const srcPath = "./src/";
const distPath = "./dist/";

const paths = {
  styles: {
    src: `${srcPath}scss/**/*.scss`,
    dest: `${distPath}css/`
  },

  scripts: {
    src: `${srcPath}js/**/*.js`,
    dest: `${distPath}js`
  },

  img:{
    src:`${srcPath}img/**/*.*`,
    dest: `${distPath}img/`
  },

  html:{
    src: `./index.html`,
    dest: distPath
  },
}

function styles() {
  return src(paths.styles.src)
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(concat("style.min.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(dest(paths.styles.dest))
    .pipe(browserSync.stream())
}

function scripts() {
  return src([paths.scripts.src])
    .pipe(concat("app.min.js"))
    .pipe(uglify())
    .pipe(dest(paths.scripts.dest))
    .pipe(browserSync.stream())
}

function imageMin() {
  return src(paths.img.src)
    .pipe(imagemin())
    .pipe(dest(paths.img.dest));
}

function html() {
  return src(paths.html.src)
    .pipe(dest(paths.html.dest))
    .pipe(browserSync.stream());
}

function watching() {
  watch([paths.styles.src], styles);
  watch([paths.scripts.src], scripts);
  watch([paths.html.src], html);
}

function reloadPage() {
  browserSync.init({
    server: {
      baseDir: distPath,
      port: 3000,
      keepaLive: true
    }
  })
}

function clean() {
  return del(distPath);
}

exports.build = series(clean, parallel(imageMin, html, styles, scripts));
exports.dev = series(parallel(reloadPage, watching));

exports.all = series(clean, parallel(styles, scripts, imageMin, html, reloadPage, watching));