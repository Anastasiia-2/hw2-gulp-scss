const menuBtn = document.querySelector(".button");
const burgerIcon = document.querySelector(".icon__open");
const closeIcon = document.querySelector(".icon__close");
const nav = document.querySelector('.navigation');

menuBtn.addEventListener("click", onClickMenuBtn);

function onClickMenuBtn(e) {
  e.target.classList.toggle("is-open");
  if (e.target.classList.contains("is-open")) {
    burgerIcon.style.display = "none";
    closeIcon.style.display = "block";
    nav.classList.add('navigation--visible');
    
  } else {
    burgerIcon.style.display = "block";
    closeIcon.style.display = "none"
    nav.classList.remove('navigation--visible');
  }  
}